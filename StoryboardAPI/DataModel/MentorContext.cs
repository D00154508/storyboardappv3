﻿using StoryboardAPI.Entities;
using System.Data.Entity;

namespace StoryboardAPI.DataModel
{
    public class MentorContext : DbContext
    {
        public DbSet<Mentor> Mentors { get; set; }
        public DbSet<Storyboard> Storyboards { get; set; }
        public DbSet<Mentee> Mentee { get; set; }
    }
}