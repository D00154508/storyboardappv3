namespace StoryboardAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Mentees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Mentors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MentorUniqueNumber = c.Int(nullable: false),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Storyboards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StickyNotes = c.String(),
                        Image = c.Binary(),
                        Mentor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mentors", t => t.Mentor_Id, cascadeDelete: true)
                .Index(t => t.Mentor_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Storyboards", "Mentor_Id", "dbo.Mentors");
            DropIndex("dbo.Storyboards", new[] { "Mentor_Id" });
            DropTable("dbo.Storyboards");
            DropTable("dbo.Mentors");
            DropTable("dbo.Mentees");
        }
    }
}
