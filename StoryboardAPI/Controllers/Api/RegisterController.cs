﻿using StoryboardAPI.DataModel;
using StoryboardAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StoryboardAPI.Controllers
{
    public class RegisterController : ApiController
    {
        private MentorContext mnContext;
        Mentor m;
        int id;
        int mentorUniqueNum;
        string name;
        string surname;
        DateTime dob;
        string username;
        string password;

        public RegisterController()
        {
            mnContext = new MentorContext();
        }

        // GET Api/Register
        public IEnumerable<Mentor> GetMentors()
        {
            return mnContext.Mentors.ToList();
        }

        // Get /Api/Register/1
        public Mentor GetMentor(int id)
        {
            var mentor = mnContext.Mentors.SingleOrDefault(m => m.Id == id);

            //return a page with error message that is user friendly
            if (mentor == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return mentor;
        }

        // Post /Api/Register
        [HttpPost]
        public Mentor AddMentor(Mentor mentor)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            mnContext.Mentors.Add(mentor);
            mnContext.SaveChanges();

            return mentor;
        }

        public void checkDB (string nm, string surnm, DateTime dateofbirth, string usernm, string pw)
        {
            mnContext.Mentors.Where(x => x.FirstName.Equals(nm));
            mnContext.Mentors.Where(x => x.SecondName.Equals(surnm));
        }

        public void addValuesToDB(string nm, string surnm, DateTime dateofbirth, string usernm, string pw)
        {
           
            m.FirstName = nm;
        }
    }
}
