﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoryboardAPI.Entities
{
    public class Mentor
    {
        public Mentor()
        {
            OwnedStoryboard = new List<Storyboard>();
        }

        public int Id { get; set; }
        public int MentorUniqueNumber { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Storyboard> OwnedStoryboard { get; set; }
    }
}