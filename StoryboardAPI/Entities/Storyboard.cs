﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoryboardAPI.Entities
{
    public class Storyboard
    {
        public int Id { get; set; }
        public string StickyNotes { get; set; }
        public byte[] Image { get; set; }
        [Required]
        public Mentor Mentor { get; set; }
    }
}